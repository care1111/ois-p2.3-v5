// Za združljivost razvoja na lokalnem računalniku ali v Cloud9 okolju
if (!process.env.PORT) {
  process.env.PORT = 8080;
}

var express = require("express");
var streznik = express();

var sqlite3 = require("sqlite3").verbose();
var pb = new sqlite3.Database("Chinook.sl3");

/**
 * Vrni seznam žanrov v povratnem klicu
 * 
 * @param povratniKlic rezultat klica funkcije, ki predstavlja tabelo vrstic
 * in v vsaki vrstici je podatek o nazivu žanra
 */
var vrniVseZanre = function(povratniKlic) {
  pb.all(
    "SELECT * FROM Genre", 
    function(napaka, vrstice) {
      var rezultat = "Prišlo je do napake!";
      if (!napaka) {
        rezultat = "<h1>" + "Žanri" + "</h1>" + "<ul>";
        vrstice.forEach(function(vrstica) {
          rezultat += "<li>" + "<a href='/kategorija/" + 
            vrstica.GenreId + "'>" + vrstica.Name + "</a>" + "</li>";
        });
        rezultat += "</ul>";
      }
      povratniKlic(rezultat);
    }
  );
};

/**
 * Vrni vse pesmi podanega žanra v povratnem klicu
 * 
 * @param idZandra ID žandra
 * @param povratniKlic rezultat klica funkcije, ki predstavlja tabelo vrstic
 * in v vsaki vrstici je podatke o nazivu žanra
 */
var vrniPesmiZanra = function(idZanra, povratniKlic) {
  pb.all(
    "SELECT * FROM Track WHERE GenreId = $id", 
    {$id: idZanra}, 
    function(napaka, vrstice) {
      var rezultat = "Prišlo je do napake!";
      if (!napaka) {
        rezultat = "<h2>" + "Pesmi" + "</h2>" + "<ul>";
        vrstice.forEach(function(vrstica) {
          rezultat += "<li>" + "<b>" + vrstica.Name + "</b>" + 
            " @ " + "$" + vrstica.UnitPrice + "</li>";
        });
      }
      povratniKlic(rezultat);
    }
  );
};

streznik.get("/", function(zahteva, odgovor) {
  vrniVseZanre(function(rezultat) {
    odgovor.send(rezultat);
  });
});

/**
 * Vrni vse pesmi, skupaj z avtorji, podanega žanra v povratnem klicu
 * 
 * @param idZandra ID žanra
 * @param povratniKlic rezultat klica funkcije, ki predstavlja tabelo vrstic
 * in v vsaki vrstici je podatke o nazivu žanra
 */
var vrniPesmiInAvtorjeZanra = function(idZanra, povratniKlic) {
  pb.all(
    "SELECT Track.Name As pesem, Artist.Name As izvajalec, \
            Track.UnitPrice AS cena \
     FROM   Track, Album, Artist \
     WHERE  Track.AlbumId = Album.AlbumId AND \
            Album.ArtistId = Artist.ArtistId AND \
            GenreId = $id", 
    {$id: idZanra}, 
    function(napaka, vrstice) {
      var rezultat = "Prišlo je do napake!";
      if (!napaka) {
        rezultat = "<h2>" + "Pesmi" + "</h2>" + "<ul>";
        vrstice.forEach(function(vrstica) {
          rezultat += "<li>" + "<b>" + vrstica.pesem + "</b>" + 
          " (" + vrstica.izvajalec + ")" +
            " @ " + "$" + vrstica.cena + "</li>";
        });
      }
      povratniKlic(rezultat);
    }
  );
};

streznik.get("/kategorija/:idKategorije", function(zahteva, odgovor) {
  vrniVseZanre(function(rezultatMaster) {
    vrniPesmiInAvtorjeZanra(zahteva.params.idKategorije, function(rezultatDetail) {
      odgovor.send(rezultatMaster + rezultatDetail);
    });
  });
});

streznik.listen(process.env.PORT, function() {
  console.log("Strežnik je pognan!");
});